module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: env('DATABASE_HOST', 'vertice-cms-dev.coajlqcgkej5.us-east-1.rds.amazonaws.com'),
        port: env.int('DATABASE_PORT', 5432),
        database: env('DATABASE_NAME', 'cms_dev_db'),
        username: env('DATABASE_USERNAME', 'cms_dev'),
        password: env('DATABASE_PASSWORD', 'vertice20'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
